FROM node:10-alpine

# Environment variables that are not secret
# Secret environment variables (i.e. username and password to access db) are in /dev/secrets
ENV CACHE_HOSTNAME=my-app-cache \
    CACHE_PORT=6379 


WORKDIR /usr/src/app

COPY package.json package-lock.json* ./

RUN apk update && \
    apk upgrade && \
    apk add --no-cache python=2.7.16-r3 && \
    apk add --no-cache make=4.2.1-r2 && \
    apk add --no-cache g++=9.2.0-r4 && \
    apk add --no-cache sqlite=3.30.1-r1 
    
RUN npm install && npm cache clean --force

COPY . .

# Launch server
CMD [ "node", "./app.js" ]
